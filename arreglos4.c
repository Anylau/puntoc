#include <stdio.h>
#include<stdint.h>

#define TAM 10

int main(void)
{
    int array[10];
for(int32_t i=0; i<TAM; i++){
array[i]=i;
}
for(int32_t i=0; i<TAM; i++){
printf(" [%d] ",array[i]);
}
printf("\n");
//array[5]=500; solucion del compañero
int *pPointer=&array[3];
*pPointer=300;
for(int32_t i=0; i<TAM; i++){
printf(" [%d] ", array[i]);
//pPointer++;//se hizo una modificacion
}
printf("\n");
array[8]=800;
//se inicializa el puntero
//pPointer=array;
for(int32_t i=0; i<TAM; i++){
printf(" [%d] ",*(pPointer+i));//desde la direccion del indice 3
}
printf("\n");
return 0;
}
