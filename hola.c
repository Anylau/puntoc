#include <stdio.h>

int main(void)
{
int *pPuntero;
int var=10;
pPuntero=&var;
printf ("Antes==> Valor de var es: [%d]\n", var );
printf ("Antes==> Valor de pPuntero es: [%d]\n", *pPuntero );

var =20;

printf ("Despues==> Valor de var es: [%d]\n", var );
printf ("Despues==> Valor de pPuntero es: [%d]\n", *pPuntero );

return 0;

}
